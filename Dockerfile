FROM mariadb:latest

ADD docker/ /docker-entrypoint-initdb.d

ENV MYSQL_ROOT_PASSWORD mypass
ENV MYSQL_DATABASE redmine
ENV MYSQL_USER redmineUser
ENV MYSQL_PASSWORD Start123

RUN apt-get update && apt-get -y upgrade && apt-get -y install vim

EXPOSE 3306

CMD ["mysqld"]