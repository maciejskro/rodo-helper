package pl.amusys.rodohelper.entity.rest;

import org.springframework.integration.annotation.Transformer;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class MailMessage  {

    private MimeMessage payload;
    private MessageHeaders mailHeaders;
    private List<String> attachment;

    public MailMessage() {
        attachment = new ArrayList<>();
    }

    @Transformer
    public MailMessage transform( MimeMessage payload , MessageHeaders headers) throws MessagingException, IOException {
        this.payload = payload;
        this.mailHeaders =  headers;
        return this;
    }

    public MimeMessage getPayload() {
        return payload;
    }

    public void setPayload(MimeMessage payload) {
        this.payload = payload;
    }

    public MessageHeaders getMailHeaders() {
        return mailHeaders;
    }

    public void setMailHeaders(MessageHeaders mailHeaders) {
        this.mailHeaders = mailHeaders;
    }
}
