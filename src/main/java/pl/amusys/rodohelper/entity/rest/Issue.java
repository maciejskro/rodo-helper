package pl.amusys.rodohelper.entity.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "project",
        "tracker",
        "status",
        "priority",
        "author",
        "assigned_to",
        "subject",
        "description",
        "start_date",
        "done_ratio",
        "spent_hours",
        "total_spent_hours",
        "custom_fields",
        "created_on",
        "updated_on"
})
public class Issue {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("project")
    @Valid
    private Project project;
    @JsonProperty("tracker")
    @Valid
    private Tracker tracker;
    @JsonProperty("status")
    @Valid
    private Status status;
    @JsonProperty("priority")
    @Valid
    private Priority priority;
    @JsonProperty("author")
    @Valid
    private Author author;
    @JsonProperty("assigned_to")
    @Valid
    private AssignedTo assignedTo;
    @JsonProperty("subject")
    private String subject;
    @JsonProperty("description")
    private String description;
    @JsonProperty("start_date")
    private String startDate;
    @JsonProperty("done_ratio")
    private Integer doneRatio;
    @JsonProperty("spent_hours")
    private Double spentHours;
    @JsonProperty("total_spent_hours")
    private Double totalSpentHours;
    @JsonProperty("custom_fields")
    @Valid
    private List<CustomField> customFields = null;
    @JsonProperty("created_on")
    private String createdOn;
    @JsonProperty("updated_on")
    private String updatedOn;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("project")
    public Project getProject() {
        return project;
    }

    @JsonProperty("project")
    public void setProject(Project project) {
        this.project = project;
    }

    @JsonProperty("tracker")
    public Tracker getTracker() {
        return tracker;
    }

    @JsonProperty("tracker")
    public void setTracker(Tracker tracker) {
        this.tracker = tracker;
    }

    @JsonProperty("status")
    public Status getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(Status status) {
        this.status = status;
    }

    @JsonProperty("priority")
    public Priority getPriority() {
        return priority;
    }

    @JsonProperty("priority")
    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    @JsonProperty("author")
    public Author getAuthor() {
        return author;
    }

    @JsonProperty("author")
    public void setAuthor(Author author) {
        this.author = author;
    }

    @JsonProperty("assigned_to")
    public AssignedTo getAssignedTo() {
        return assignedTo;
    }

    @JsonProperty("assigned_to")
    public void setAssignedTo(AssignedTo assignedTo) {
        this.assignedTo = assignedTo;
    }

    @JsonProperty("subject")
    public String getSubject() {
        return subject;
    }

    @JsonProperty("subject")
    public void setSubject(String subject) {
        this.subject = subject;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("start_date")
    public String getStartDate() {
        return startDate;
    }

    @JsonProperty("start_date")
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    @JsonProperty("done_ratio")
    public Integer getDoneRatio() {
        return doneRatio;
    }

    @JsonProperty("done_ratio")
    public void setDoneRatio(Integer doneRatio) {
        this.doneRatio = doneRatio;
    }

    @JsonProperty("spent_hours")
    public Double getSpentHours() {
        return spentHours;
    }

    @JsonProperty("spent_hours")
    public void setSpentHours(Double spentHours) {
        this.spentHours = spentHours;
    }

    @JsonProperty("total_spent_hours")
    public Double getTotalSpentHours() {
        return totalSpentHours;
    }

    @JsonProperty("total_spent_hours")
    public void setTotalSpentHours(Double totalSpentHours) {
        this.totalSpentHours = totalSpentHours;
    }

    @JsonProperty("custom_fields")
    public List<CustomField> getCustomFields() {
        return customFields;
    }

    @JsonProperty("custom_fields")
    public void setCustomFields(List<CustomField> customFields) {
        this.customFields = customFields;
    }

    @JsonProperty("created_on")
    public String getCreatedOn() {
        return createdOn;
    }

    @JsonProperty("created_on")
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    @JsonProperty("updated_on")
    public String getUpdatedOn() {
        return updatedOn;
    }

    @JsonProperty("updated_on")
    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}