package pl.amusys.rodohelper.entity.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "project",
        "group",
        "roles",
        "user",
        "group_anonymous"
})
public class Membership {

    @JsonProperty("id")
    private int id;
    @JsonProperty("project")
    @Valid
    private Project project;
    @JsonProperty("group")
    @Valid
    private Group group;
    @JsonProperty("roles")
    @Valid
    private List<Role> roles = null;
    @JsonProperty("user")
    @Valid
    private User user;
    @JsonProperty("group_anonymous")
    @Valid
    private GroupAnonymous groupAnonymous;
    @JsonIgnore
    @Valid
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public int getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(int id) {
        this.id = id;
    }

    @JsonProperty("project")
    public Project getProject() {
        return project;
    }

    @JsonProperty("project")
    public void setProject(Project project) {
        this.project = project;
    }

    @JsonProperty("group")
    public Group getGroup() {
        return group;
    }

    @JsonProperty("group")
    public void setGroup(Group group) {
        this.group = group;
    }

    @JsonProperty("roles")
    public List<Role> getRoles() {
        return roles;
    }

    @JsonProperty("roles")
    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    @JsonProperty("user")
    public User getUser() {
        return user;
    }

    @JsonProperty("user")
    public void setUser(User user) {
        this.user = user;
    }

    @JsonProperty("group_anonymous")
    public GroupAnonymous getGroupAnonymous() {
        return groupAnonymous;
    }

    @JsonProperty("group_anonymous")
    public void setGroupAnonymous(GroupAnonymous groupAnonymous) {
        this.groupAnonymous = groupAnonymous;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}