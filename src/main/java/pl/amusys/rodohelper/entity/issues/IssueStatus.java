package pl.amusys.rodohelper.entity.issues;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Table(name = "issue_statuses")
public class IssueStatus implements Serializable {

    @Id
    private Integer id;
    private String name;
    private Integer isClosed;
    private Integer position;
    private Integer defaultDoneRatio;

}
