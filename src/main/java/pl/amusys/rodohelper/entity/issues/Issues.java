package pl.amusys.rodohelper.entity.issues;


import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.Nullable;
import pl.amusys.rodohelper.entity.contacts.Contacts;
import pl.amusys.rodohelper.entity.helpdesk.HelpdeskTickets;
import pl.amusys.rodohelper.entity.projects.Projects;
import pl.amusys.rodohelper.entity.projects.Trackers;
import pl.amusys.rodohelper.entity.users.Users;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@Entity
@Table(name = "issues")
public class Issues  implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;
  @OneToOne
  @JoinColumn(name="tracker_id")
  private Trackers trackerId;
  @ManyToOne
  @JoinColumn(name = "project_id")
  private Projects projectId;
  private String subject;
  private String description;
  @Nullable
  private LocalDate dueDate;
  @OneToOne
  @JoinColumn(name = "category_id" , nullable = true)
  @Nullable
  private IssueCategories categoryId;
  @OneToOne
  @JoinColumn(name = "status_id")
  private IssueStatus statusId;

  @ManyToOne
  @JoinColumn(name = "assigned_to_id", nullable = true)
  @Nullable
  private Users assignedToId;
  private long priorityId;
  @Nullable
  private Long fixedVersionId;
  private Long authorId;
  private long lockVersion;
  private LocalDateTime createdOn;
  private LocalDateTime updatedOn;
  private LocalDate startDate;
  private long doneRatio;
  @Nullable
  private Double estimatedHours;
  @OneToOne
  @JoinColumn(name = "parent_id", nullable = true)
  private Issues parentId;
  private long rootId;
  private long lft;
  private long rgt;
  private long isPrivate;
  @Nullable
  private LocalDateTime closedOn;
  @OneToOne(mappedBy = "issueId")
  private HelpdeskTickets helpdeskTickets;

  @ManyToMany
  @JoinTable(name = "contacts_issues" , joinColumns = @JoinColumn(name = "issue_id") ,
            inverseJoinColumns = @JoinColumn(name = "contact_id"))
  private List<Contacts> contacts_issues;
}
