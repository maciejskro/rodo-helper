package pl.amusys.rodohelper.entity.issues;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@NoArgsConstructor
@Table(name = "issue_relations")
public class IssueRelations implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;
  private long issueFromId;
  private long issueToId;
  private String relationType;
  private long delay;
}
