package pl.amusys.rodohelper.entity.issues;

import lombok.Data;
import lombok.NoArgsConstructor;
import pl.amusys.rodohelper.entity.contacts.Contacts;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
@Table(name = "journal_messages")
public class JournalMessages {

  @Id
  private long id;
  @OneToOne
  @JoinColumn(name = "contact_id")
  private Contacts contactId;
  @OneToOne
  @JoinColumn(name = "journal_id")
  private Journals journalId;
  private long isIncoming;
  private long source;
  private String fromAddress;
  private String toAddress;
  private String bccAddress;
  private String ccAddress;
  private LocalDateTime messageDate;
  private String messageId;

}
