package  pl.amusys.rodohelper.entity.issues;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@NoArgsConstructor
@Table(name = "issue_categories")
public class IssueCategories implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;
  private long projectId;
  private String name;
  private long assignedToId;


}
