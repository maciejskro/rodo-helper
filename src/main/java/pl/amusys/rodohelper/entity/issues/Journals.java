package pl.amusys.rodohelper.entity.issues;

import lombok.Data;
import lombok.NoArgsConstructor;
import pl.amusys.rodohelper.entity.users.Users;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
@Table(name = "journals")
public class Journals {

  @Id
  private long id;
  @OneToOne
  @JoinColumn(name = "journalized_id")
  private Issues journalizedId;
  private String journalizedType;
  @OneToOne
  @JoinColumn(name = "user_id")
  private Users userId;
  private String notes;
  private LocalDateTime createdOn;
  private long privateNotes;

}
