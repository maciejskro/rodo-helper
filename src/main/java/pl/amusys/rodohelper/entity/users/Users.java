package pl.amusys.rodohelper.entity.users;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.amusys.rodohelper.entity.contacts.EmailAddresses;
import pl.amusys.rodohelper.entity.issues.Issues;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "users")
@Builder
@AllArgsConstructor
public class Users implements Serializable {

  @Id
  private int id;
  private String login;
  private String hashedPassword;
  private String firstname;
  private String lastname;
  private long admin;
  private long status;
  private LocalDateTime lastLoginOn;
  private String language;
  private Long authSourceId;
  private LocalDateTime createdOn;
  private LocalDateTime updatedOn;
  @Enumerated(EnumType.STRING)
  private UserType type;
  private String identityUrl;
  private String mailNotification;
  private String salt;
  private long mustChangePasswd;
  private LocalDateTime passwdChangedOn;

  @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
  private List<EmailAddresses> emailsList = new ArrayList<>();

  @OneToMany(mappedBy = "assignedToId")
  private List<Issues> assignedTicket = new ArrayList<>();
  @OneToMany(mappedBy = "userId")
  private List<Members> membersInProject = new ArrayList<>();


  public Users (Users u) {
    this.id = u.getId();
    this.login = u.getLogin();
    this.hashedPassword = u.getHashedPassword();
    this.firstname = u.getFirstname();
    this.lastname = u.getLastname();
    this.admin = u.getAdmin();
    this.status = u.getStatus();
    this.lastLoginOn = u.getLastLoginOn();
    this.authSourceId = u.getAuthSourceId();
    this.createdOn = u.getCreatedOn();
    this.updatedOn = u.getUpdatedOn();
    this.type = u.getType();
    this.identityUrl = u.getIdentityUrl();
    this.salt = u.getSalt();
    this.mustChangePasswd = u.getMustChangePasswd();
    this.passwdChangedOn = u.getPasswdChangedOn();
  }

}
