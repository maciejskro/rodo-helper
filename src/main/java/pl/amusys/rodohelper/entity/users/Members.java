package pl.amusys.rodohelper.entity.users;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import pl.amusys.rodohelper.entity.projects.Projects;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Table(name = "members")
public class Members implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @ManyToOne
  @JoinColumn(name = "user_id", referencedColumnName = "id")
  private Users userId;
  @ManyToOne
  @JoinColumn(name="project_id", referencedColumnName = "id")
  private Projects projectId;

  private LocalDateTime createdOn;
  private long mailNotification;
  private String events;

}
