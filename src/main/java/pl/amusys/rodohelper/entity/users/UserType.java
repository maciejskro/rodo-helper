package pl.amusys.rodohelper.entity.users;

public enum UserType {

    User, Group , GroupAnonymous, AnonymousUser

}
