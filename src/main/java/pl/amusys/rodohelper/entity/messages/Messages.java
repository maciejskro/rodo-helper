package pl.amusys.rodohelper.entity.messages;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
@Table(name = "messages")
public class Messages {

  @Id
  private long id;
  private long boardId;
  private long parentId;
  private String subject;
  private String content;
  private long authorId;
  private long repliesCount;
  private long lastReplyId;
  private LocalDateTime createdOn;
  private LocalDateTime updatedOn;
  private long locked;
  private long sticky;

}
