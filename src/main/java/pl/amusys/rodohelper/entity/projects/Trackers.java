package pl.amusys.rodohelper.entity.projects;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import pl.amusys.rodohelper.entity.issues.Issues;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Table(name = "trackers")
public class Trackers implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;
  @Column(name = "name")
  private String name;
  @Column(name = "is_in_chlog")
  private long isInChlog;
  @Column(name = "position")
  private long position;
  @Column(name = "is_in_roadmap")
  private long isInRoadmap;
  @Column(name = "fields_bits")
  private long fieldsBits;
  @Column(name = "default_status_id")
  private long defaultStatusId;

  @ManyToMany(mappedBy = "listTrackers" , fetch = FetchType.LAZY)
  private List<Projects> listProject = new ArrayList<>();

  @OneToMany(fetch = FetchType.LAZY)
  private List<Issues> listIssues = new ArrayList<>();
}
