package pl.amusys.rodohelper.entity.projects;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.lang.Nullable;
import pl.amusys.rodohelper.entity.issues.Issues;
import pl.amusys.rodohelper.entity.settings.ContactsSettings;
import pl.amusys.rodohelper.entity.settings.Settings;
import pl.amusys.rodohelper.entity.users.Members;
import pl.amusys.rodohelper.entity.users.Users;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Table(name = "projects")
public class Projects implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;
  @Column(name = "name")
  private String name;
  @Column(name = "description")
  private String description;
  @Column(name="homepage")
  private String homepage;
  @Column(name = "is_public")
  private long isPublic;
  @Column(name = "parent_id")
  @Nullable
  private Long parentId;
  @Column(name = "created_on")
  @Temporal(TemporalType.TIMESTAMP)
  private java.util.Date createdOn;
  @Column(name = "updated_on")
  @Temporal(TemporalType.TIMESTAMP)
  private java.util.Date updatedOn;
  @Column(name = "identifier")
  private String identifier;
  @Column(name = "status")
  private long status;
  @Column(name = "lft")
  private long lft;
  @Column(name = "rgt")
  private long rgt;
  @Column(name = "inherit_members")
  private long inheritMembers;
  @Column(name = "default_version_id")
  @Nullable
  private Long defaultVersionId;
  @Column(name = "format_store")
  private String formatStore;
  @OneToOne
  @JoinColumn(name = "default_assigned_to_id")
  @Nullable
  private Users defaultAssignedToId;

  @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
  @JoinTable(name = "projects_trackers",
        joinColumns = {@JoinColumn(name = "project_id" ,referencedColumnName = "id")},
        inverseJoinColumns = {@JoinColumn(name = "tracker_id",referencedColumnName = "id")}
      )
  private List<Trackers> listTrackers = new ArrayList<>();

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "projectId")
  private List<Issues> issuesList = new ArrayList<>();

  @OneToMany(mappedBy = "projectId",
         fetch = FetchType.EAGER)
  private List<ContactsSettings> projSettingsList = new ArrayList<>();

  @OneToMany(mappedBy = "projectId",
          fetch = FetchType.LAZY,
          cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private  List<Members> listMemberOfProject = new ArrayList<>();
}
