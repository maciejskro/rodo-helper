package pl.amusys.rodohelper.entity.helpdesk;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.amusys.rodohelper.entity.contacts.Contacts;
import pl.amusys.rodohelper.entity.issues.Issues;

@Entity
@Data
@NoArgsConstructor
@Table(name = "helpdesk_tickets")
public class HelpdeskTickets implements Serializable {

    private static final long serialVersionUID = 1238622093929530775L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @OneToOne
    @JoinColumn(name = "contact_id", referencedColumnName = "id")
    private Contacts contactId;
    @OneToOne
    @JoinColumn(name = "issue_id", referencedColumnName = "id")
    private Issues issueId;
    private long source;
    private String fromAddress;
    private String toAddress;
    private LocalDateTime ticketDate;
    private String ccAddress;
    private String messageId;
    private long isIncoming;
    private long reactionTime;
    private long firstResponseTime;
    private long resolveTime;
    private LocalDateTime lastAgentResponseAt;
    private LocalDateTime lastCustomerResponseAt;
    private long vote;
    private String voteComment;

}
