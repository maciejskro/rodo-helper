package pl.amusys.rodohelper.entity.contacts;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
@Table(name = "addresses")
public class Addresses {

  @Id
  private long id;
  private String street1;
  private String street2;
  private String city;
  private String region;
  private String postcode;
  private String countryCode;
  private String fullAddress;
  private String addressType;
  private long addressableId;
  private String addressableType;
  private LocalDateTime createdAt;
  private LocalDateTime updatedAt;


}
