package pl.amusys.rodohelper.entity.contacts;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import pl.amusys.rodohelper.entity.users.Users;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Table(name = "email_addresses")
public class EmailAddresses implements Serializable {

  @Id
  private int id;
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "user_id", nullable = false)
  private Users userId;
  private String address;
  private long isDefault;
  private long notify;
  private LocalDateTime createdOn;
  private LocalDateTime updatedOn;
}
