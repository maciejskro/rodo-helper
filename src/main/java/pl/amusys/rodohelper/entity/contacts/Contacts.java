package pl.amusys.rodohelper.entity.contacts;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.ToString.Exclude;
import org.hibernate.proxy.HibernateProxy;
import pl.amusys.rodohelper.entity.issues.Issues;
import pl.amusys.rodohelper.entity.users.Users;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@NoArgsConstructor
@Table(name = "contacts")
public class Contacts implements Serializable {

    private static final long serialVersionUID = 5578424748060862842L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String firstName;
    private String lastName;
    private String middleName;
    private String company;
    private String phone;
    private String email;
    private String website;
    private String skypeName;
    private LocalDate birthday;
    private String avatar;
    private String background;
    private String jobTitle;
    private long isCompany;
    @OneToOne
    @JoinColumn(name = "author_id")
    private Users authorId;
    private long assignedToId;
    private LocalDateTime createdOn;
    private LocalDateTime updatedOn;
    private String cachedTagList;
    private long visibility;

    @ManyToMany(mappedBy = "contacts_issues")
    @Exclude
    private List<Issues> contactsIssues;

    @Override
    public final boolean equals(Object object) {
        if(this == object) {
            return true;
        }
        if(object == null) {
            return false;
        }
        Class<?> oEffectiveClass =
            object instanceof HibernateProxy ? ((HibernateProxy) object).getHibernateLazyInitializer()
                .getPersistentClass() : object.getClass();
        Class<?> thisEffectiveClass =
            this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer()
                .getPersistentClass() : this.getClass();
        if(thisEffectiveClass != oEffectiveClass) {
            return false;
        }
        Contacts contacts = (Contacts) object;
        return Objects.equals(getId(), contacts.getId());
    }

    @Override
    public final int hashCode() {
        return this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass().hashCode()
                                              : getClass().hashCode();
    }
}
