package pl.amusys.rodohelper.entity.settings;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
@Table(name = "settings")
public class Settings implements Serializable {

  @Id
  private long id;
  private String name;
  private String value;
  private LocalDateTime updatedOn;

}
