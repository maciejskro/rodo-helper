package pl.amusys.rodohelper.entity.settings;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import pl.amusys.rodohelper.entity.projects.Projects;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode
@Table(name = "contacts_settings")
public class ContactsSettings implements Serializable {

  @Id
  private long id;
  private String name;
  private String value;
  private LocalDateTime updatedOn;
  @ManyToOne
  @JoinColumn(name = "project_id")
  private Projects projectId;

}
