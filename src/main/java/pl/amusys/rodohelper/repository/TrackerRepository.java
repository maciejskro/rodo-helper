package pl.amusys.rodohelper.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.amusys.rodohelper.entity.projects.Projects;
import pl.amusys.rodohelper.entity.projects.Trackers;

import java.util.List;

@Repository
public interface TrackerRepository extends JpaRepository<Trackers,Long> {

    Trackers findByName(String name);

   // @Query ("select t from Trackers t join fetch t.listProject ")
    List<Trackers> findAllByListProjectIsIn (@Param("projId") Projects projId);
}
