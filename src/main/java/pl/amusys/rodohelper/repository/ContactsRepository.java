package pl.amusys.rodohelper.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.amusys.rodohelper.entity.contacts.Contacts;

import java.util.List;

@Repository
public interface ContactsRepository extends JpaRepository<Contacts, Long>  {

    List<Contacts> getAllByEmail(String email);
}
