package pl.amusys.rodohelper.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.amusys.rodohelper.entity.issues.IssueStatus;

import java.util.List;

@Repository
public interface IssueStatusRepository extends JpaRepository<IssueStatus, Integer> {

    List<IssueStatus> findAll();
    List<IssueStatus> findAllByName(String name);
    IssueStatus findByPositionEquals(Integer i);
}
