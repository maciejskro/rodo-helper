package pl.amusys.rodohelper.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.amusys.rodohelper.entity.projects.Projects;
import pl.amusys.rodohelper.entity.settings.ContactsSettings;

import java.util.Optional;

@Repository
public interface ContactSettingsRepository extends JpaRepository<ContactsSettings, Long> {

    Optional<ContactsSettings> getByName(String name);
    Optional<ContactsSettings> getAllByNameAndProjectId(String name , Projects projects);
}
