package pl.amusys.rodohelper.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.amusys.rodohelper.entity.issues.Issues;


@Repository
public interface IssueRepository extends JpaRepository <Issues, Long> {

}
