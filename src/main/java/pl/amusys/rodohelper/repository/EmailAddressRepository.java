package pl.amusys.rodohelper.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.amusys.rodohelper.entity.contacts.EmailAddresses;

import java.util.Optional;

@Repository
public interface EmailAddressRepository  extends JpaRepository<EmailAddresses, Long> {

    Optional<EmailAddresses> findByAddress(String email);

}
