package pl.amusys.rodohelper.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.amusys.rodohelper.entity.settings.Settings;

import java.util.Optional;

@Repository
public interface SettingsRepository extends JpaRepository<Settings,Long> {

    Optional<Settings> getByName(String name);
}
