package pl.amusys.rodohelper.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.amusys.rodohelper.entity.projects.Projects;
import pl.amusys.rodohelper.entity.projects.Trackers;

import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Projects, Long> {

    @Query("select p from Projects p join fetch p.listMemberOfProject " +
            " where p.identifier = (:name)")
    Projects findAllByIdentifier(@Param("name") String name);

}
