package pl.amusys.rodohelper.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.amusys.rodohelper.entity.helpdesk.HelpdeskTickets;

@Repository
public interface HelpdeskTicketRepository extends JpaRepository<HelpdeskTickets, Long> {

}
