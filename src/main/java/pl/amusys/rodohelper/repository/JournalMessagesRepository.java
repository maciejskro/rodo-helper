package pl.amusys.rodohelper.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.amusys.rodohelper.entity.issues.JournalMessages;

@Repository
public interface JournalMessagesRepository extends JpaRepository<JournalMessages, Long> {
}
