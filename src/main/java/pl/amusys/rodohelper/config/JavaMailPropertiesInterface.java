package pl.amusys.rodohelper.config;


import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Properties;

@ConfigurationProperties(prefix="spring.mail")
public interface JavaMailPropertiesInterface {

    String getMailHost();

    Integer getMailPort();

    String getUsername();

    String getPassword();

    String getStoreProtocol();

    String getSmtpMailHost();

    Integer getSmtpMailPort();

    String getSmtpUsername();

    String getSmtpPassword();

   // @Value("${spring.mail.properties.mail.smtp.auth}")
    boolean isSmtpAuth();

   // @Value("${spring.mail.properties.mail.smtp.starttls.enable}")
    boolean isSmtpStartTLS();


    Properties getSmtpSendProperties();
}
