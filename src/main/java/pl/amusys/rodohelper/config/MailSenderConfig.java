package pl.amusys.rodohelper.config;

import java.util.Properties;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.mail.dsl.Mail;
import org.springframework.integration.mail.dsl.MailSendingMessageHandlerSpec;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Configuration
@EnableIntegration
@ComponentScan(value = "pl.amusys.rodohelper")
@RequiredArgsConstructor
public class MailSenderConfig {

    private final JavaMailProperties javaMailPropertiesInterface;

    @Bean
    public SimpleMailMessage defaultMessage() {
        return new SimpleMailMessage();
    }
    @Bean
    public MailSender getMail2Sender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        Properties props = javaMailPropertiesInterface.getSmtpSendProperties();
        mailSender.setJavaMailProperties(props);
        return mailSender;
    }

    @Bean
    MailSendingMessageHandlerSpec getMailSender () {
        return Mail.outboundAdapter(javaMailPropertiesInterface.getMailHost())
                .port(javaMailPropertiesInterface.getMailPort())
                .protocol(javaMailPropertiesInterface.getStoreProtocol())
                .credentials(javaMailPropertiesInterface.getUsername(), javaMailPropertiesInterface.getPassword())
                .javaMailProperties(javaMailPropertiesInterface.getSmtpSendProperties())
                .defaultEncoding("UTF-8");
    }
}
