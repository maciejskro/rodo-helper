package pl.amusys.rodohelper.config;

import com.taskadapter.redmineapi.RedmineManager;
import com.taskadapter.redmineapi.RedmineManagerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.util.Collections;

@Configuration
public class RedmineConfig {


    @Value("${redmine.uri}")
    private String redmineURI;

    @Value("${redmine.apikey}")
    private String apikey;

    @Bean
    public RedmineManager redmineManager() {
        return RedmineManagerFactory.createWithApiKey(redmineURI,apikey);
    }

    @Bean
    public HttpHeaders getHttpHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON_UTF8));
            httpHeaders.setContentType(MediaType.APPLICATION_JSON);
            httpHeaders.set("X-Redmine-API-Key", apikey);
        return httpHeaders;
    }
}
