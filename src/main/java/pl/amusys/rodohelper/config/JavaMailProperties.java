package pl.amusys.rodohelper.config;

import java.util.Properties;
import javax.annotation.PostConstruct;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import pl.amusys.rodohelper.entity.projects.Projects;
import pl.amusys.rodohelper.services.SettingsServices;

@Getter
@Setter
@Component
public class JavaMailProperties {

    private String mailHost;
    private Integer mailPort;
    private String username;
    private String password;
    private boolean useSSL;

    private String storeProtocol;

    private String smtpMailHost;
    private Integer smtpMailPort;
    private String smtpUsername;
    private String smtpPassword;
    private boolean smtpAuth;
    private boolean smtpStartTLS;
    private boolean smtpSSL;
    private String imapFolder;

    private final Projects currentProjects;

    private final SettingsServices settingsServices;

    public JavaMailProperties(@Qualifier("currentProject") Projects currentProjects,
                              SettingsServices settingsServices) {
        this.currentProjects = currentProjects;
        this.settingsServices = settingsServices;
    }

    @PostConstruct
    public void  init() {
        //imap settings
        mailHost = settingsServices.getSmtpTransportSettings("helpdesk_host", currentProjects);
        mailPort = Integer.parseInt(settingsServices.getSmtpTransportSettings("helpdesk_port",currentProjects));
        username = settingsServices.getSmtpTransportSettings("helpdesk_username", currentProjects);
        password = settingsServices.getSmtpTransportSettings("helpdesk_password", currentProjects);
        imapFolder = settingsServices.getSmtpTransportSettings("helpdesk_imap_folder", currentProjects);
        useSSL = (settingsServices.getSmtpTransportSettings("helpdesk_use_ssl", currentProjects)).equals("1") ;
        storeProtocol = useSSL? settingsServices.getSmtpTransportSettings("helpdesk_protocol", currentProjects) +"s"
                        : settingsServices.getSmtpTransportSettings("helpdesk_protocol", currentProjects) ;

        // smtp settings
        smtpMailHost = settingsServices.getSmtpTransportSettings("helpdesk_smtp_server", currentProjects);
        smtpMailPort = Integer.parseInt(settingsServices.getSmtpTransportSettings("helpdesk_smtp_port", currentProjects));
        smtpUsername = settingsServices.getSmtpTransportSettings("helpdesk_smtp_username", currentProjects);
        smtpPassword = settingsServices.getSmtpTransportSettings("helpdesk_smtp_password", currentProjects);

        smtpStartTLS = (settingsServices.getSmtpTransportSettings("helpdesk_smtp_tls",currentProjects)).equals("1");
        smtpSSL = (settingsServices.getSmtpTransportSettings("helpdesk_smtp_ssl",currentProjects)).equals("1");

/*

        helpdesk_smtp_use_default_settings	1
        helpdesk_smtp_domain	amusys.pl
        helpdesk_smtp_authentication	plain

*/
    }

    public String getMailConnector() {

        return this.storeProtocol+"://"
                +this.username.replace("@","%40")
                +":"
                +this.password
                +"@"
                +this.mailHost
                +":"
                +this.mailPort
                +"/"+
                this.imapFolder;
    }

    public Properties getSmtpSendProperties() {
        Properties p = new Properties();
        p.put("mail.smtp.host", getSmtpMailHost());
        p.put("mail.smtp.port", getSmtpMailPort());
        p.put("mail.smtp.auth", isSmtpAuth());
        p.put("mail.smtp.starttls.enable", isSmtpAuth());
        p.put("mail.store.protocol", getStoreProtocol());

        return p;
    }
}
