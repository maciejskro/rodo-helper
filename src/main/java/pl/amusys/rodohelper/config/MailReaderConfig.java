package pl.amusys.rodohelper.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.Pollers;
import org.springframework.integration.mail.ImapIdleChannelAdapter;
import org.springframework.integration.mail.ImapMailReceiver;
import org.springframework.integration.scheduling.PollerMetadata;
import org.springframework.messaging.Message;
import org.springframework.web.client.RestTemplate;
import pl.amusys.rodohelper.entity.projects.Projects;
import pl.amusys.rodohelper.services.ProjectsServices;
import pl.amusys.rodohelper.services.UserService;

import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeMessage;
import java.io.IOException;

@Configuration
@EnableIntegration
@ComponentScan(value = "pl.amusys.rodohelper")
@Getter
public class MailReaderConfig {

    @Value("${redmine.project.name_follow}")
    private String followedProject;

    @Autowired
    private  JavaMailProperties javaMailProper;

    @Autowired
    private ProjectsServices projectsServices;

    @Autowired
    private UserService userService;

    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    @Qualifier("currentProject")
    public Projects getCurrentProjects() {
        return projectsServices.getProject(followedProject);
    }

    private boolean logMail(Message payload) {
        System.out.println(payload.getHeaders().toString());
        return true;
    }

    private boolean hasAttachments(MimeMessage message) throws MessagingException, IOException {
        if (message.isMimeType("multipart/mixed")) {
            Multipart mp = (Multipart) message.getContent();
            if (mp.getCount() > 1) {
                return true;
            }
        }
        return false;
    }

    @Bean(name = PollerMetadata.DEFAULT_POLLER)
    public PollerMetadata pollerMetadata() {
        return Pollers.fixedDelay(100)
                    .maxMessagesPerPoll(1).get();
    }

    //@Bean
    //@ServiceActivator(inputChannel = "mailChannel")
    @InboundChannelAdapter
    public ImapIdleChannelAdapter mailAdapter() {

        ImapMailReceiver mailReceiver =  new ImapMailReceiver(javaMailProper.getMailConnector());
        mailReceiver.setJavaMailProperties(javaMailProper.getSmtpSendProperties());
        mailReceiver.setShouldMarkMessagesAsRead(true);
        mailReceiver.setCancelIdleInterval(100L);
        mailReceiver.afterPropertiesSet();
        return new ImapIdleChannelAdapter(mailReceiver);
    }


}
