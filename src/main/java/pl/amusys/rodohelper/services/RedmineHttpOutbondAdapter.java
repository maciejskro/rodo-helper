package pl.amusys.rodohelper.services;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.http.outbound.HttpRequestExecutingMessageHandler;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;

@MessageEndpoint
@Configuration
@RequiredArgsConstructor
public class RedmineHttpOutbondAdapter   {

    @Value("${redmine.uri}")
    private String redmineURL;

    @Value("${redmine.apikey}")
    private String apikey;

    @Bean
    public MessageHandler outbound() {

        HttpRequestExecutingMessageHandler handler = new HttpRequestExecutingMessageHandler(redmineURL+ "/issues/" + 2 + "json" );
        handler.setHttpMethod(HttpMethod.PUT);
        handler.setExpectedResponseType(String.class);

        return handler;
    }

    @Bean
    public MessageChannel directMessage() {
        return new QueueChannel();
    }

}
