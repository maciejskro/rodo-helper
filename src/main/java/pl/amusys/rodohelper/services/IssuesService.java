package pl.amusys.rodohelper.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.amusys.rodohelper.entity.contacts.Contacts;
import pl.amusys.rodohelper.entity.issues.IssueStatus;
import pl.amusys.rodohelper.entity.issues.Issues;
import pl.amusys.rodohelper.entity.projects.Projects;
import pl.amusys.rodohelper.entity.projects.Trackers;
import pl.amusys.rodohelper.entity.users.Users;
import pl.amusys.rodohelper.repository.HelpdeskTicketRepository;
import pl.amusys.rodohelper.repository.IssueRepository;
import pl.amusys.rodohelper.repository.UserRepository;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

@Service
public class IssuesService {

    @Autowired
    private IssueRepository issueRepo;

    @Value("${redmine.project.name_follow}")
    private  String projectName;

    @Autowired
    private ProjectsServices projectService;
    @Autowired
    private ContactServices contactServices;
    @Autowired
    private EmailService emailService;
    @Autowired
    private  IssueStatusService issueStatusService;
    @Autowired
    private UserRepository userRepository;


    @Autowired
    private Supplier<LocalDateTime> localDateTime;



    Optional<Issues> createIssueTicket(MimeMessage m) throws MessagingException, IOException {
        Optional<Issues> i;
        Map<String,Contacts> c = contactServices.getProperlyContact(m);
        Projects p = projectService.getProject(projectName);
        Optional<Trackers> t = p.getListTrackers().stream().filter( trackers -> trackers.getName().equals("Support"))
                .findFirst();
        Integer issueId = emailService.getIssueIDfromSubject(m);
        Users u = userRepository.findByLogin("CustomerGDPR");
        if (issueId > 0 ) {
            i = issueRepo.findById(Long.parseLong(issueId.toString() ) );
            //upgrade issue with issueId
        } else {
            Issues issue = getIssuesBuild(t, p, m, u ,issueStatusService.getLessIssueStatus(2));
            i = Optional.of( issueRepo.save(issue) ) ;
        }
        return i;
    }

    Optional<Issues> createNewIssueTicket(MimeMessage m ,Optional<Users> u) throws MessagingException, IOException{
        Optional<Issues> i;
        Projects p = projectService.getProject(projectName);
        Optional<Trackers> t = p.getListTrackers().stream().filter( trackers -> trackers.getName().equals("Support"))
                .findFirst();
        Integer issueId = emailService.getIssueIDfromSubject(m);
        if (issueId > 0 ) {
            i = issueRepo.findById(Long.parseLong(issueId.toString() ) );
            //upgrade issue with issueId
        } else {
            Issues issue = getIssuesBuild(t, p, m, u.orElse(null),issueStatusService.getNamedIssueStatus("New").get());
            i = Optional.of( issueRepo.save(issue) ) ;
        }
        return i;
    }

    private Issues getIssuesBuild(Optional<Trackers> t, Projects p , MimeMessage m, Users u , IssueStatus is)
            throws MessagingException, IOException {
        Issues result = new Issues();
                result.setTrackerId( t.get() );
                result.setProjectId(p);
                result.setSubject(m.getSubject());
                //TODO there may be other kind of contentType !!!!
                result.setDescription(m.getContent().toString());
                result.setCreatedOn(LocalDateTime.now());
                result.setUpdatedOn(LocalDateTime.now());
                result.setStatusId(is);
                result.setAssignedToId(u);
                result.setAuthorId(4L);
                result.setStartDate(localDateTime.get().toLocalDate());
                result.setIsPrivate(0L);
                result.setLft(1L);
                result.setRgt(2L);
                return result;
    }

    public Optional<Issues> findByID( Long id) {
        return issueRepo.findById(id);

    }
}
