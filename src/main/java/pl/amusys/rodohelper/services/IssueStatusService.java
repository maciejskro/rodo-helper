package pl.amusys.rodohelper.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.amusys.rodohelper.entity.issues.IssueStatus;
import pl.amusys.rodohelper.repository.IssueStatusRepository;

import java.util.List;
import java.util.Optional;

@Service
public class IssueStatusService {

    @Autowired
    private IssueStatusRepository issueStatusRepository;

    public List<IssueStatus> getIssueStatus () {
        return issueStatusRepository.findAll();
    }

    public IssueStatus getLessIssueStatus(Integer i) {
        return issueStatusRepository.findByPositionEquals(i);
    }

    public Optional<IssueStatus> getNamedIssueStatus(String name) {
        return issueStatusRepository.findAllByName(name).stream().findFirst() ;
    }

}
