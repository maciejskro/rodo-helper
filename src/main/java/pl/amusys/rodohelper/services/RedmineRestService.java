package pl.amusys.rodohelper.services;

import com.taskadapter.redmineapi.Include;
import com.taskadapter.redmineapi.RedmineException;
import com.taskadapter.redmineapi.RedmineManager;
import com.taskadapter.redmineapi.bean.Issue;
import com.taskadapter.redmineapi.bean.Project;
import com.taskadapter.redmineapi.bean.Tracker;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.amusys.rodohelper.entity.issues.Issues;
import pl.amusys.rodohelper.repository.IssueRepository;
import pl.amusys.rodohelper.repository.ProjectRepository;
import pl.amusys.rodohelper.repository.TrackerRepository;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class RedmineRestService {

    private static Log logger = LogFactory.getLog(RedmineRestService.class);

    @Value("${redmine.uri}")
    private String redmineURI;

    @Value("${redmine.apikey}")
    private String apikey;

    @Value("${redmine.project.name_follow}")
    private  String projectName;

    @Autowired
    private RedmineManager mgr;

    @Autowired
    private IssueRepository issueRepo;
    @Autowired
    private ProjectRepository projectRepo;
    @Autowired
    private TrackerRepository trackerRepo;

    @Autowired
    RestTemplate redmineRestTempl;

    @Autowired
    private ProjectsServices projectService;


    public List<com.taskadapter.redmineapi.bean.Project> getListProjects() {
        try {
            return mgr.getProjectManager().getProjects();
        } catch (RedmineException re)
        {
            return (List<Project>) Optional.empty().get();
        }
       /* HttpEntity<String> entity = new HttpEntity<>("parameters", httpHeaders);
        ResponseEntity<com.taskadapter.redmineapi.bean.Project> test = redmineRestTempl.exchange(redmineURI + "/projects.json", HttpMethod.GET, entity, com.taskadapter.redmineapi.bean.Project.class);
        return test.getBody();*/
    }

    public List<com.taskadapter.redmineapi.bean.Issue> getIssueInProject(String projName) {
        com.taskadapter.redmineapi.bean.Project p = getListProjects().stream()
                .filter((project) -> project.getName().equals(projName))
                .collect(Collectors.toList())
                .get(0);
        try {
            List<Issue> listIssue = mgr.getIssueManager().getIssues(projectName, 0, Include.relations, Include.children, Include.watchers);
            return listIssue;
        } catch (RedmineException re) {
            logger.error("Cannot get list of issue in GDPR project");
            return (List<Issue>)Optional.empty().get();
        }
    }

    Collection<Tracker> getAvailableTracker() {
        try {
           // System.out.println( projectService.getAvailableTrackerForProject().stream().findFirst().toString()) ;
            System.out.println("get available tracker");
            List<com.taskadapter.redmineapi.bean.Project> list = mgr.getProjectManager().getProjects();
            int proj_id  = list.stream()
                               .filter( p -> ( p.getName().equals(projectName.toUpperCase()) || p.getName().equals(projectName.toLowerCase())) )
                               .collect(Collectors.toList())
                    .get(0).getId();
            return list.stream().filter( p -> p.getId() == proj_id)
                            .collect(Collectors.toList()).get(0).getTrackers();
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    public boolean isProjectMember(String projName, String userEmail) throws RedmineException {
        List<com.taskadapter.redmineapi.bean.Membership> members = mgr.getMembershipManager().getMemberships(projName);
        Optional<com.taskadapter.redmineapi.bean.User> user = mgr.getUserManager().getUsers().stream().filter(u -> u.getMail().equals(userEmail))
                .findFirst();
        for (com.taskadapter.redmineapi.bean.Membership m : members) {
            if (user.isPresent() && m.getUserId() == user.get().getId()) {
                return true;
            }
        }
        return false;
    }

    public boolean isIssueProject(String projName, Integer issueId) throws RedmineException {
        try {
            Optional<com.taskadapter.redmineapi.bean.Issue> issue = Optional.of(mgr.getIssueManager().getIssueById(issueId));
            if (issue.isPresent()) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public Integer getProjectIdFromIssueId(Integer issue_id) {
        Integer result = 0;
        try {
            Optional<com.taskadapter.redmineapi.bean.Issue> issue = Optional.of(mgr.getIssueManager().getIssueById(issue_id));
            if (issue.isPresent()) {
                result = issue.get().getProjectId();
            }
        } catch (RedmineException re) {
            result = 0;
        }
        return result;
    }

    public boolean updateIssueById(Integer issue_id, Message payload) throws MessagingException, IOException {
        com.taskadapter.redmineapi.bean.Issue issue;
        try {
            issue = mgr.getIssueManager().getIssueById(issue_id, Include.relations);
        } catch (RedmineException re) {
            return false;
        }
        issue.setAuthorName(payload.getFrom()[0].toString());
        issue.setSubject(payload.getSubject());

        if (!payload.isMimeType("multipart/*")) {
            issue.setNotes(payload.getContent().toString());
        } else {
            MimeMultipart mimeMultipart = (MimeMultipart) payload.getContent();
            issue.setNotes(getTextFromMimeMultipart(mimeMultipart));
        }
        try {
            mgr.getIssueManager().update(issue);
            System.out.println(issue.getRelations());
        } catch (RedmineException re) {
            return false;
        }
        return true;
    }

    public boolean createHelpdeskIssue(Message payload) {
        com.taskadapter.redmineapi.bean.Issue issue = new com.taskadapter.redmineapi.bean.Issue();
        List<Project> projectList = getListProjects();
        try {
            Issues i = new Issues();
                    i.setTrackerId(trackerRepo.findByName("Support"));
                   // i.setProjectId(projectRepo.findAllByIdentifier("gdpr"));
                    i.setSubject(payload.getSubject());
                    i.setDescription(payload.getContent().toString());
            issue.setProjectId(projectList.stream().filter(project -> project.getName().equals("gdpr") || project.getName().equals("GDPR"))
                    .findFirst().get().getId());
            issue.setSubject(payload.getSubject().toString());

            issue.setTracker(getAvailableTracker().stream()
                                                  .filter( t -> t.getName().toLowerCase().equals("support"))
                                                  .findFirst().get());
            if (payload.getContentType().contains("text")) {
                issue.setDescription((String) payload.getContent());
            } else
                issue.setDescription(getTextFromMimeMultipart((MimeMultipart) payload.getContent()));

            issue.setAssigneeName("CustomerGDPR");


           // mgr.getIssueManager().createIssue(issue);

        } catch (Exception re) {
            System.out.println("exception catch ");
            return false;
        }
        return true;
    }

    private String getTextFromMimeMultipart(MimeMultipart mimeMultipart) throws MessagingException, IOException {
        String result = "";
        int count = mimeMultipart.getCount();
        for (int i = 0; i < count; i++) {
            BodyPart body = mimeMultipart.getBodyPart(i);
            if (body.isMimeType("text/plain")) {
                result = result + "\n" + body.getContent();
                break;
            } else if (body.isMimeType("text/html")) {
                String html = (String) body.getContent();
                result = result + "\n" + org.jsoup.Jsoup.parse(html).text();
            } else if (body.getContent() instanceof MimeMultipart) {
                result = result + getTextFromMimeMultipart((MimeMultipart) body.getContent());
            }
        }
        return result;
    }

    private Date getDateNow() {
        LocalDateTime ldt = LocalDateTime.now();
        ZonedDateTime zdt = ldt.atZone(ZoneId.systemDefault());
        return Date.from(zdt.toInstant());
    }

    private void checkExistingContactInProject(String contactMail) {

        //redmineRestTemp
    }
}

