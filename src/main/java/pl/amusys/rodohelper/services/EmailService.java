package pl.amusys.rodohelper.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.amusys.rodohelper.entity.contacts.Contacts;
import pl.amusys.rodohelper.entity.issues.Issues;
import pl.amusys.rodohelper.entity.projects.Projects;
import pl.amusys.rodohelper.entity.projects.Trackers;
import pl.amusys.rodohelper.entity.users.Users;

import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class EmailService {

    @Autowired
    private  IssuesService issuesService;

    @Autowired
    private ContactServices contactServices;

    @Autowired
    private UserService userService;

    @Autowired
    private  HelpdeskService helpdeskService;


    @Autowired
    private ProjectsServices projectsServices;

    @Value("${redmine.projectname}")
    private String followedProject;

    public String getMailFromField(String payload) {
        Optional<String> result = Optional.of(payload.substring(payload.indexOf("<")+1,payload.lastIndexOf(">")));
        if (result.isPresent()) {
            return result.get();
        } else {
            return "unable get mail string";
        }
    }
    int getIssueIDfromSubject(MimeMessage subject) throws MessagingException {
        Optional<Integer> result = null;
        String toParse = subject.getSubject();
        Pattern pattern1 = Pattern.compile("\\[*\\s#\\d+\\]");
        Matcher matcher = pattern1.matcher(toParse);
        if (matcher.find()) {
            result= Optional.of( Integer.parseInt( toParse.substring( matcher.start()+1,matcher.end() ) ));
            if (result.isPresent()) {
                return  result.get();
            }
            return -1;
        }
        return -1;
    }

    Optional<Issues> getIssueFromSubject(MimeMessage m) throws MessagingException {
        Integer issueId = getIssueIDfromSubject(m);
        Optional<Issues> i = issuesService.findByID(issueId.longValue());
        return i;
    }


    String getFromEmailAddress(MimeMessage m) throws MessagingException {
        Address[] adr = m.getFrom();
        String email = adr == null ? null : ((InternetAddress) adr[0]).getAddress();
        return email;
    }

    String getToEmailAddress(MimeMessage m) throws MessagingException {
        Address[] adr = m.getReplyTo();
        String result = adr == null ? null : ((InternetAddress) adr[0]).getAddress();
        return result;
    }

    String getPersonalNameIntenetAddress( MimeMessage m) throws MessagingException {
        Address[] adr = m.getFrom();
        String person =  adr==null ? null : ((InternetAddress) adr[0]).getPersonal();
        return person;
    }



    public void makeDecision (MimeMessage payload )  throws MessagingException, IOException {

        Projects projects = projectsServices.getProject(followedProject);
        Optional<Users> u = userService.getUserWithEmail(payload);
        Map<String, Contacts> c = contactServices.getProperlyContact(payload);
        List<Users> me =  projectsServices.getMemberOfProject(projects);
        Collection<Trackers> tr = projectsServices.getAvailableTrackerForProject(projects);

        Optional<Issues> i = getIssueFromSubject(payload);
        if (u.isPresent()) {
            if (i.isPresent()) {
                // mail od member of project z tematem
                // dodajemy do ticketa kolejny wpis
                // !!!! wysyłamy mail do contact and other users in project.
            } else  {
                // mail od member of project bez wskazania na ticket
                // tworzymy ticket i łączymy z userem
                // !!! wysyłamy to other user in project.
            }
        } else {
            if (c.containsKey("new")) {
                //  kontakt nie ma w bazie contact
                // tworzymy kontakt - var "c" is created contact
                Contacts newContact = c.get("new");
                if (i.isPresent()) {
                    //mail zawiera jakims cudem nr ticketa
                } else {
                    Optional<Issues> createdIssue = issuesService.createNewIssueTicket(payload, userService.getAnonymousUser());
                    helpdeskService.createHelpdesTicket(createdIssue.get(),payload,newContact);

                }
                // tworzymy issue
                // tworzymy helpdesk_ticket
                // łączymy issue, kontakt i helpdeskticekt
                // mail to member of project
            } if ( c.containsKey("exist")) {
                // kontakt istneje w bazie
                if ( i.isPresent() ) {
                    // odpowiedz do istniejacego ticketa
                    // dodajemy do issue wpis
                    // mail to member of project
                } else {
                    // nowy temat
                    // tworzymy issue
                    // tworzymy helpdesk_ticket
                    // łączymy issue, kontakt i helpdeskticket
                    // mail to member of project
                }
            }
            else {
                // nieznana akcja - exception
            }
        }
    }

      /*
           2. sprawdź czy FROM istniej w tabeli users, jak tak to pobierz usera.             UserService.getUserWithEmail()
           1. sprawdż czy FROM istnieje w table contacts, jak nie to załuż contact i oddaj - ContactService.getPropertyContact flaga string = new
                                                          jak tak to pobierz -               ContactService.getPropertyContact flaga string = exist
           3. sprawdź czy temat zawiera [*#344*] jeśli tak to poszukaj issues                 EmailService.getIssueFromSubject

           --- analiza maila ---

           4. jeśli pole from jest z innego kontakut niż w tabeli contact AND innego niż w tabeli user to :
                     a. jeśli temat nie zawiera \\[*#\\d+\\] to I. tworzymy contact
                                                                II. tworzmy ticket ( issue i helpdesk_ticket)
                     b. jeśli temat zawiera \\[*#\\d+\\] to szukaj issue. Jeśli jest to dopisz do mail do jourals
                                                                   Jeśli nie to stwórz nowy ticket (issue i helpdesk_ticket)
           5. jeśli pole from jest z adresu cotact  to :
                     a. jeśli temat nie zawiera \\[*#\\d+\\] to tworzmy ticket ( issue i helpdesk_ticket) wiązemy ticket z kontaktem
                     b. jeśli temat zawiera \\[*#\\d+\\] to szukaj issue. Jeśli jest to dopisz do mail do jourals
                                                                   Jeśli nie to stwórz nowy ticket (issue i helpdesk_ticket)
           6. Jesli pole form jest z adresu user to:
                     a. jeśli temat nie zawiera \\[*#\\d+\\] to tworzmy ticket ( issue i helpdesk_ticket)
                     b. jeśli temat zawiera \\[*#\\d+\\] to szukaj issue. Jeśli jest to dopisz do mail do jourals
                                                                   Jeśli nie to stwórz nowy ticket (issue i helpdesk_ticket)

     */
}

