package pl.amusys.rodohelper.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.amusys.rodohelper.entity.contacts.EmailAddresses;
import pl.amusys.rodohelper.entity.users.Users;
import pl.amusys.rodohelper.repository.EmailAddressRepository;
import pl.amusys.rodohelper.repository.UserRepository;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    private EmailAddressRepository eaRepository;


    public Optional<Users> getAnonymousUser() {
        return  userRepository.findAllByType("AnonymousUser").stream()
                .filter(u -> u.getLastname().equals("Anonymous"))
                .findFirst();
    }

    public Optional<Users> getUserWithEmail(MimeMessage m) throws MessagingException {
        String email = emailService.getFromEmailAddress(m);
        Optional<EmailAddresses> ea = eaRepository.findByAddress(email);
      return Optional.ofNullable(ea.isPresent() ? ea.get().getUserId() : null);
    }

}
