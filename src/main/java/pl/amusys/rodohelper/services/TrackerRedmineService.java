package pl.amusys.rodohelper.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.amusys.rodohelper.entity.projects.Projects;
import pl.amusys.rodohelper.entity.projects.Trackers;
import pl.amusys.rodohelper.repository.TrackerRepository;

import java.util.List;

@Service
public class TrackerRedmineService {

    @Autowired
    private TrackerRepository trackerRepository;

    public List<Trackers> getTrackerList() {
        return trackerRepository.findAll();
    }

    public Trackers getTrackerNamed(String named) {
        return trackerRepository.findByName(named);
    }

    public List<Trackers> getTrackerByProject(Projects projID) {
        return trackerRepository.findAllByListProjectIsIn(projID);
    }
}
