package pl.amusys.rodohelper.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.amusys.rodohelper.entity.contacts.Contacts;
import pl.amusys.rodohelper.repository.ContactsRepository;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ContactServices {

    @Autowired
    private ContactsRepository contactRepo;

    @Autowired
    private UserService userService;

    @Autowired
    private EmailService emailService;

    private List<Contacts> getGivenContact(String email) {
        return contactRepo.getAllByEmail(email);
    }

    public Contacts saveContact (Contacts c) {
        if ( ! c.equals(null) ) {
            return contactRepo.save(c);
        } else
            return null;
    }

    public Map<String,Contacts> getProperlyContact (MimeMessage m) throws MessagingException {
        Map<String,Contacts> result = new HashMap<>();
        String email = emailService.getFromEmailAddress(m);
        List<Contacts>  contacts = getGivenContact(email);
        Contacts c ;
        if ( contacts.isEmpty() ) {
            String person = emailService.getPersonalNameIntenetAddress(m);
            c = new Contacts();
                 c.setEmail(email);
                 c.setFirstName(person.split(" ").length>0 ? person.split(" ")[0]: null);
                 c.setLastName(person.split(" ").length>1 ? person.substring( person.indexOf(" ")+1 ) : null);
                 c.setCompany(email.split("@")[1]);
                 c.setAuthorId(userService.getAnonymousUser().isPresent()? userService.getAnonymousUser().get() : null);
                 c.setCreatedOn(LocalDateTime.now());
                 c.setUpdatedOn(LocalDateTime.now());
            c = saveContact(c);
            result.put("new", c);
        } else {
            c = getGivenContact(email).get(0);
            result.put("exist",c);
        }
       return result;
    }
}
