package pl.amusys.rodohelper.services;

import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.amusys.rodohelper.entity.projects.Projects;
import pl.amusys.rodohelper.entity.settings.ContactsSettings;
import pl.amusys.rodohelper.entity.settings.Settings;
import pl.amusys.rodohelper.repository.ContactSettingsRepository;
import pl.amusys.rodohelper.repository.SettingsRepository;

@Service
@RequiredArgsConstructor
public class SettingsServices {

    private final ContactSettingsRepository contactSettingsRepository;

    private final SettingsRepository settingsRepository;


    public String getNamedSettings(String name, Projects projects) {
        Settings result = null;
        ContactsSettings result2 = null;
        if ( projects == null ) {
            Optional<Settings> r = settingsRepository.getByName(name);
             result = r.isPresent() ? r.get() : null;
        } else {
            result2 = contactSettingsRepository.getAllByNameAndProjectId(name, projects).get();
        }
        return result != null ? result.getValue() : result2.getValue();
    }

    public String getSmtpTransportSettings(String name, Projects projects) {
        Optional<ContactsSettings> settings = projects.getProjSettingsList().stream().filter(x -> x.getName().equals(name))
                        .findFirst();
        return settings.map(ContactsSettings::getValue).orElse(null);
    }

}
