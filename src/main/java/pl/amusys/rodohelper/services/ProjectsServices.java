package pl.amusys.rodohelper.services;

import java.util.List;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.amusys.rodohelper.entity.projects.Projects;
import pl.amusys.rodohelper.entity.projects.Trackers;
import pl.amusys.rodohelper.entity.users.Members;
import pl.amusys.rodohelper.entity.users.Users;
import pl.amusys.rodohelper.repository.ProjectRepository;

@Service
@RequiredArgsConstructor
public class ProjectsServices {

    private final ProjectRepository projectRepo;

    private final TrackerRedmineService trackerService;

    @Value("${redmine.project.name_follow}")
    private String followedProject;

    public List<Trackers> getAvailableTrackerForProject(Projects proj) {
        List<Trackers> tr = trackerService.getTrackerByProject(proj);
        return tr;
    }

    public Projects getProject(String name ) {
        return projectRepo.findAllByIdentifier(name);
    }

    public List<Users> getMemberOfProject(Projects proj) {
        return proj.getListMemberOfProject().stream()
                .map(Members::getUserId )
                .collect(Collectors.toList());
    }

}
