package pl.amusys.rodohelper.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.amusys.rodohelper.entity.contacts.Contacts;
import pl.amusys.rodohelper.entity.helpdesk.HelpdeskTickets;
import pl.amusys.rodohelper.entity.issues.Issues;
import pl.amusys.rodohelper.repository.HelpdeskTicketRepository;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.time.LocalDateTime;

@Service
public class HelpdeskService {

    @Autowired
    private HelpdeskTicketRepository helpdeskTicketRepository;

    @Autowired
    private EmailService emailService;

    public HelpdeskTickets createHelpdesTicket(Issues i, MimeMessage m, Contacts c) throws MessagingException {
        HelpdeskTickets hdt = new HelpdeskTickets();
                hdt.setContactId(c);
                hdt.setIssueId(i);
                hdt.setFromAddress(emailService.getFromEmailAddress(m));
                hdt.setToAddress(emailService.getToEmailAddress(m));
                hdt.setTicketDate(LocalDateTime.now());
                hdt.setIsIncoming(1L);
        return saveHelpDeskTicket(hdt);
    }

    public HelpdeskTickets saveHelpDeskTicket(HelpdeskTickets hdt) {
        return helpdeskTicketRepository.save(hdt);
    }
}
