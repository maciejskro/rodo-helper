package pl.amusys.rodohelper.dto;


import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import pl.amusys.rodohelper.entity.contacts.Contacts;
import pl.amusys.rodohelper.entity.helpdesk.HelpdeskTickets;

@Builder
@Getter
@Setter
public class IssuesDto implements Serializable {

    @Id
    @NotNull
    private long id;
    @NotNull
    private int trackerId;
    @NotNull
    private int projectId;
    private String subject;
    private String description;
    private LocalDate dueDate;
    private int categoryId;
    private int statusId;
    private int assignedToId;
    private int priorityId;
    private int fixedVersionId;
    private int authorId;
    private int lockVersion;
    private LocalDateTime createdOn;
    private LocalDateTime updatedOn;
    private LocalDate startDate;
    private long doneRatio;
    private double estimatedHours;
    private IssuesDto parentId;
    private long rootId;
    private long lft;
    private long rgt;
    private long isPrivate;
    private LocalDateTime closedOn;


    @OneToOne(mappedBy = "issueId")
    private HelpdeskTickets helpdeskTickets;

    @ManyToMany
    @JoinTable(name = "contacts_issues", joinColumns = @JoinColumn(name = "issue_id"),
        inverseJoinColumns = @JoinColumn(name = "contact_id"))
    private List<Contacts> contactsIssues;
}
