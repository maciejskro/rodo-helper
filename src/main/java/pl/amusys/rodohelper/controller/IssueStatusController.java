package pl.amusys.rodohelper.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.amusys.rodohelper.entity.issues.IssueStatus;
import pl.amusys.rodohelper.repository.IssueStatusRepository;

import java.util.List;
import java.util.Optional;

@RestController
public class IssueStatusController {

    @Autowired
    private IssueStatusRepository statusRepository;

    @GetMapping("/v1/issues/statuses")
    public Optional<List<IssueStatus>> getListStatus() {
        return  Optional.of(statusRepository.findAll());
    }


}
