package pl.amusys.rodohelper.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.amusys.rodohelper.entity.issues.Issues;
import pl.amusys.rodohelper.repository.IssueRepository;

import java.util.List;
import java.util.Optional;

@RestController
public class IssueRestController {

    @Autowired
    private IssueRepository issueRepository;

    @GetMapping("/v1/issues/list")
    public Optional<List<Issues>> getListIssues() {
        return Optional.of(issueRepository.findAll());
    }

}
