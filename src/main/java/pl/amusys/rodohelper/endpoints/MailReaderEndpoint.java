package pl.amusys.rodohelper.endpoints;

import java.util.Collection;
import javax.mail.internet.MimeMessage;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.stereotype.Component;
import pl.amusys.rodohelper.config.MailReaderConfig;
import pl.amusys.rodohelper.entity.issues.Issues;
import pl.amusys.rodohelper.entity.projects.Trackers;
import pl.amusys.rodohelper.services.IssuesService;

@Component
@EnableIntegration
@AllArgsConstructor
public class MailReaderEndpoint {

    private final MailReaderConfig mailReaderConfig;

    private final IssuesService issuesService;

    @Bean
    public IntegrationFlow integrationFlow() {

        return IntegrationFlows.from(mailReaderConfig.mailAdapter() )

                .<MimeMessage>handle((payload, header) -> {
                            try {
                                Collection<Trackers> tr = mailReaderConfig.getProjectsServices().getAvailableTrackerForProject(mailReaderConfig.getCurrentProjects());

                                if ( ! tr.isEmpty() ) {
                                    System.out.println("filter working");

                                    // redmineRestService.isIssueProject("test", 216);
                                    System.out.println("support");
                                    Issues i = issuesService.findByID(133L).get();
                                    System.out.println(i.getCreatedOn());
                                    return true;

                                } else
                                    return true;

                                //edmineRestService.createHelpdeskIssue(payload);
                                //return payload;
                            } catch (Exception e) {
                                e.printStackTrace();
                                return " null mail";
                            }
                        }
                )
                .channel("mailChannel")
                .get();
    }
}
