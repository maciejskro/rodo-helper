package pl.amusys.rodohelper.endpoints;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.MessageChannels;
import pl.amusys.rodohelper.config.MailSenderConfig;

@ComponentScan
@Configuration
@EnableIntegration
@RequiredArgsConstructor
public class MailSendEndpoint  {

    private final MailSenderConfig mailSenderConfig;

    @Bean
    public IntegrationFlow sendSimpleMessage () {
        return IntegrationFlows.from(MessageChannels.queue("mailChannel"))
                .handle(p -> p.getPayload().toString())
                .get();

    }
}
