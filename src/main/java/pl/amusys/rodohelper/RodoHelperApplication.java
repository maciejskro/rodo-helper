package pl.amusys.rodohelper;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.config.EnableIntegration;

import java.time.LocalDateTime;
import java.util.function.Supplier;

@SpringBootApplication
@EnableIntegration
@Configuration
public class RodoHelperApplication {

    private static ConfigurableApplicationContext ctx;

    @Value("${redmine.projectname}")
    private String followProject;

    public static void main(String[] args) throws Exception{
        ctx = new SpringApplicationBuilder(RodoHelperApplication.class)
               .web(WebApplicationType.NONE)
                .run(args);
        //SpringApplication.run(RodoHelperApplication.class, args);
        System.out.println("Hit enter to terminate");
        System.in.read();
        //ctx.close();
    }

    @Bean
    public Supplier<LocalDateTime> localDateTimeSupplier() {
        return  LocalDateTime:: now;
    }
}
